package items;

import heroes.ArmorType;
import heroes.Slot;
import org.junit.jupiter.api.Test;
import utils.HeroAttribute;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {

    @Test
    void weaponCreation_correctParam_shouldPass() {
        //arrange
        HeroAttribute ha = new HeroAttribute();
        Armor expected = new Armor("Common Plate Chest", 1, Slot.BODY, ArmorType.PLATE, ha);

        //act
        Armor actual = new Armor("Common Plate Chest", 1, Slot.BODY, ArmorType.PLATE, ha);

        //assert
        assertTrue(expected.equals(actual));
    }

}