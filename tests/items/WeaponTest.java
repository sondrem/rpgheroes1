package items;

import heroes.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {

    @Test
    void weaponCreation_correctParam_shouldPass() {
        //arrange
        Weapon expected = new Weapon("Common Axe", 1, WeaponType.AXE, 2);

        //act
        Weapon actual = new Weapon("Common Axe", 1, WeaponType.AXE, 2);

        //assert
        assertTrue(expected.equals(actual));
    }

}