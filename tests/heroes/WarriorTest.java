package heroes;

import org.junit.jupiter.api.Test;
import utils.HeroAttribute;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    @Test
    void warriorCreation_correctName_shouldPass() {
        //arrange
        String expected = "Captain America";
        //act
        Warrior warrior = new Warrior(expected);
        String actual = warrior.getName();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void warriorCreation_correctLevel_shouldPass() {
        //arrange
        String name = "Captain America";
        int expected = 1;
        //act
        Warrior warrior = new Warrior(name);
        int actual = warrior.getLevel();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void warriorCreation_correctAttributes_shouldPass() {
        //arrange
        String name = "Captain America";
        HeroAttribute expected = new HeroAttribute(5,2,1);
        //act
        Warrior warrior = new Warrior(name);
        HeroAttribute actual = warrior.getLevelAttributes();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_validAttributes_shouldPass() {
        //arrange
        HeroAttribute expected = new HeroAttribute(8,4,2);
        String name = "Captain America";
        Warrior warrior= new Warrior(name);
        //act
        warrior.levelUp();
        HeroAttribute actual = warrior.getLevelAttributes();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void warriorLevelUp_validLevel_shouldPass() {
        //arrange
        String name = "Captain America";
        int actual = 2; //1+1
        Warrior warrior = new Warrior(name);
        //act
        warrior.levelUp();
        int expected = warrior.getLevel();
        //assert
        assertEquals(expected, actual);
    }
}