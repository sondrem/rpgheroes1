package heroes;

import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import items.Armor;
import items.Weapon;
import org.junit.jupiter.api.Test;
import utils.HeroAttribute;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {

    @Test
    void testEquipArmor_validInput_shouldPass() throws InvalidArmorException {
        //arrange
        Warrior warrior = new Warrior("Captain America");
        HeroAttribute ha = new HeroAttribute();
        Armor expected = new Armor("Common Plate Chest", 1, Slot.BODY, ArmorType.PLATE, ha);

        //act
        warrior.equipArmor(expected);
        Armor actual = warrior.getArmor(Slot.BODY);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void testEquipArmor_tooLowLevel_shouldThrowInvalidArmorException() {
        //arrange
        Warrior warrior = new Warrior("Captain America");
        HeroAttribute ha = new HeroAttribute();
        Armor armor = new Armor("Common Plate Chest", 2, Slot.BODY, ArmorType.PLATE, ha);
        String expected = "Captain America is too low level to equip this armor!";

        //act
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> warrior.equipArmor(armor));
        String actual = exception.getMessage();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void testEquipArmor_invalidArmorType_shouldThrowInvalidArmorException() {
        //arrange
        Warrior warrior = new Warrior("Captain America");
        HeroAttribute ha = new HeroAttribute();
        Armor armor = new Armor("Common Shirt", 1, Slot.BODY, ArmorType.CLOTH, ha);
        String expected = "Captain America cannot equip this type of armor!";

        //act
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> warrior.equipArmor(armor));
        String actual = exception.getMessage();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void testEquipWeapon_validInput_shouldPass() throws InvalidWeaponException {
        //arrange
        Warrior warrior = new Warrior("Captain America");
        HeroAttribute ha = new HeroAttribute();
        Weapon expected = new Weapon("Common Axe", 1, WeaponType.AXE, 2);

        //act
        warrior.equipWeapon(expected);
        Weapon actual = warrior.getWeapon();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void testEquipWeapon_invalidWeaponType_shouldThrowInvalidWeaponException() {
        //arrange
        Warrior warrior = new Warrior("Captain America");
        HeroAttribute ha = new HeroAttribute();
        Weapon weapon = new Weapon("Common Wand", 1, WeaponType.WAND, 2);
        String expected = "Captain America cannot equip this type of weapon";

        //act
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> warrior.equipWeapon(weapon));
        String actual = exception.getMessage();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void testEquipWeapon_tooLowLevel_shouldThrowInvalidWeaponException() {
        //arrange
        Warrior warrior = new Warrior("Captain America");
        HeroAttribute ha = new HeroAttribute();
        Weapon weapon = new Weapon("Common Axe", 2, WeaponType.AXE, 2);
        String expected = "Captain America is too low level to equip this weapon";

        //act
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> warrior.equipWeapon(weapon));
        String actual = exception.getMessage();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void testTotalAttributes_noEquipment_shouldPass() {
        //arrange
        Warrior warrior = new Warrior("Captain America");
        HeroAttribute expected = new HeroAttribute(5,2,1);

        //act
        HeroAttribute actual = warrior.getLevelAttributes();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void testTotalAttributes_validArmorEquipped_shouldPass() throws InvalidArmorException {
        //arrange
        Warrior warrior = new Warrior("Captain America");
        HeroAttribute ha = new HeroAttribute(2,0,0);
        Armor armor = new Armor("Common Plate Chest", 1, Slot.BODY, ArmorType.PLATE, ha);
        int expected = (5+2+1+2);

        //act
        warrior.equipArmor(armor);
        int actual = warrior.totalAttributes();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void testTotalAttributes_twoValidArmorsEquipped_shouldPass() throws InvalidArmorException {
        //arrange
        Warrior warrior = new Warrior("Captain America");
        HeroAttribute ha = new HeroAttribute(2,0,0);
        Armor armor = new Armor("Common Plate Chest", 1, Slot.BODY, ArmorType.PLATE, ha);
        Armor secondArmor = new Armor("Common Plate Leg", 1, Slot.LEGS, ArmorType.PLATE, ha);
        int expected = (5+2+1+2+2);

        //act
        warrior.equipArmor(armor);
        warrior.equipArmor(secondArmor);
        int actual = warrior.totalAttributes();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void testTotalAttributes_replacedArmorEquipped_shouldPass() throws InvalidArmorException {
        //arrange
        Warrior warrior = new Warrior("Captain America");
        HeroAttribute ha = new HeroAttribute(2,0,0);
        Armor armor = new Armor("Common Plate Chest", 1, Slot.BODY, ArmorType.PLATE, ha);
        HeroAttribute haSecond = new HeroAttribute(4,0,0);
        Armor secondArmor = new Armor("Super Plate Chest", 1, Slot.BODY, ArmorType.PLATE, haSecond);
        int expected = (5+2+1+4);

        //act
        warrior.equipArmor(armor);
        warrior.equipArmor(secondArmor);
        int actual = warrior.totalAttributes();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void testDamage_noWeaponEquipped_shouldPass() throws InvalidWeaponException {
        //arrange
        Warrior warrior = new Warrior("Captain America");
        int expected =  1*(1 + (5 / 100));

        //act
        int actual = warrior.damage();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void testDamage_weaponEquipped_shouldPass() throws InvalidWeaponException {
        //arrange
        Warrior warrior = new Warrior("Captain America");
        Weapon weapon = new Weapon("Common Axe", 1, WeaponType.AXE, 2);
        int expected =  2*(1 + (5 / 100));

        //act
        warrior.equipWeapon(weapon);
        int actual = warrior.damage();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void testDamage_replaceOneWeapon_shouldPass() throws InvalidWeaponException {
        //arrange
        Warrior warrior = new Warrior("Captain America");
        Weapon weapon = new Weapon("Common Axe", 1, WeaponType.AXE, 2);
        Weapon secondWeapon = new Weapon("Common Hammer", 1, WeaponType.HAMMER, 4);
        int expected =  4*(1 + (5 / 100));

        //act
        warrior.equipWeapon(weapon);
        warrior.equipWeapon(secondWeapon);
        int actual = warrior.damage();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void testDamage_weaponAndArmorEquipped_shouldPass() throws InvalidWeaponException, InvalidArmorException {
        //arrange
        Warrior warrior = new Warrior("Captain America");
        Weapon weapon = new Weapon("Common Axe", 1, WeaponType.AXE, 2);
        HeroAttribute ha = new HeroAttribute(1,0,0);
        Armor armor = new Armor("Common Plate Chest", 1, Slot.BODY, ArmorType.PLATE, ha);
        int expected =  2*(1 + ((5+1) / 100));

        //act
        warrior.equipWeapon(weapon);
        warrior.equipArmor(armor);
        int actual = warrior.damage();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    void testDisplay_validInput_shouldPass() {
        //arrange
        Warrior warrior = new Warrior("Captain America");
        String expected = "Your hero: {Name='Captain America', Class=Warrior, Level=1," +
                " Total strength=5," +
                " Total dexterity=2, Total intelligence=1, Damage=1}";

        //act
        String actual = warrior.display();

        //assert
        assertEquals(expected, actual);
    }
}