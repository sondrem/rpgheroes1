package heroes;

import org.junit.jupiter.api.Test;
import utils.HeroAttribute;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {

    @Test
    void rangerCreation_correctName_shouldPass() {
        //arrange
        String expected = "Aragorn";
        //act
        Ranger ranger = new Ranger(expected);
        String actual = ranger.getName();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void rangerCreation_correctLevel_shouldPass() {
        //arrange
        String name = "Aragorn";
        int expected = 1;
        //act
        Ranger ranger = new Ranger(name);
        int actual = ranger.getLevel();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void rangerCreation_correctAttributes_shouldPass() {
        //arrange
        String name = "Aragorn";
        HeroAttribute expected = new HeroAttribute(1,7,1);
        //act
        Ranger ranger = new Ranger(name);
        HeroAttribute actual = ranger.getLevelAttributes();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void rangerLevelUp_validAttributes_shouldPass() {
        //arrange
        HeroAttribute expected = new HeroAttribute(2,12,2);
        String name = "Aragorn";
        Ranger ranger= new Ranger(name);
        //act
        ranger.levelUp();
        HeroAttribute actual = ranger.getLevelAttributes();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void rangerLevelUp_validLevel_shouldPass() {
        //arrange
        String name = "Aragorn";
        int actual = 2; //1+1
        Ranger ranger = new Ranger(name);
        //act
        ranger.levelUp();
        int expected = ranger.getLevel();
        //assert
        assertEquals(expected, actual);
    }
}