package heroes;

import org.junit.jupiter.api.Test;
import utils.HeroAttribute;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {

    @Test
    void mageCreation_correctName_shouldPass() {
        //arrange
        String expected = "Gandalf";
        //act
        Mage mage = new Mage(expected);
        String actual = mage.getName();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void mageCreation_correctLevel_shouldPass() {
        //arrange
        String name = "Gandalf";
        int expected = 1;
        //act
        Mage mage = new Mage(name);
        int actual = mage.getLevel();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void mageCreation_correctAttributes_shouldPass() {
        //arrange
        String name = "Gandalf";
        HeroAttribute expected = new HeroAttribute(1,1,8);

        //act
        Mage mage = new Mage(name);
        HeroAttribute actual = mage.getLevelAttributes();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void mageLevelUp_validAttributes_shouldPass() {
        //arrange
        HeroAttribute expected = new HeroAttribute(2,2,13);
        String name = "Gandalf";
        Mage mage = new Mage(name);
        //act
        mage.levelUp();
        HeroAttribute actual = mage.getLevelAttributes();
        //assert
        assertEquals(expected, actual);
    }

    @Test
    void mageLevelUp_validLevel_shouldPass() {
        //arrange
        String name = "Gandalf";
        int actual = 2; //1+1
        Mage mage = new Mage(name);
        //act
        mage.levelUp();
        int expected = mage.getLevel();
        //assert
        assertEquals(expected, actual);
    }
}