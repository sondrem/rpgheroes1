package heroes;

public class Ranger extends Hero {

    public Ranger(String name) {
        super(name);
        levelAttributes.increaseStrength(1);
        levelAttributes.increaseDexterity(7);
        levelAttributes.increaseIntelligence(1);
        validWeaponTypes.add(WeaponType.BOW);
        validArmorTypes.add(ArmorType.LEATHER);
        validArmorTypes.add(ArmorType.MAIL);
    }

    @Override
    public void levelUp() {
        increaseAttributes(1, 5, 1);
    }

    @Override
    public int specialityDamage() {
        return levelAttributes.getDexterity();
    }
}
