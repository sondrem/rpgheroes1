package heroes;

public class Rogue extends Hero {

    public Rogue(String name) {
        super(name);
        levelAttributes.increaseStrength(2);
        levelAttributes.increaseDexterity(6);
        levelAttributes.increaseIntelligence(1);
        validWeaponTypes.add(WeaponType.DAGGER);
        validWeaponTypes.add(WeaponType.SWORD);
        validArmorTypes.add(ArmorType.LEATHER);
        validArmorTypes.add(ArmorType.MAIL);
    }

    @Override
    public void levelUp() {
        increaseAttributes(1, 4, 1);
    }

    @Override
    public int specialityDamage() {
        return levelAttributes.getDexterity();
    }
}
