package heroes;

public class Mage extends Hero {

    public Mage(String name) {
        super(name);
        levelAttributes.increaseStrength(1);
        levelAttributes.increaseDexterity(1);
        levelAttributes.increaseIntelligence(8);
        validWeaponTypes.add(WeaponType.STAFF);
        validWeaponTypes.add(WeaponType.WAND);
        validArmorTypes.add(ArmorType.CLOTH);
    }

    @Override
    public void levelUp() {
        increaseAttributes(1, 1, 5);
    }


    @Override
    public int specialityDamage() {
        return levelAttributes.getIntelligence();
    }
}
