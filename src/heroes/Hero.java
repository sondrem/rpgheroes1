package heroes;

import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import items.Armor;
import items.Item;
import items.Weapon;
import utils.HeroAttribute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public abstract class Hero {

    private final String name;
    protected int level;
    protected HeroAttribute levelAttributes;
    protected HashMap<Slot, Item> equipment;
    protected ArrayList<WeaponType> validWeaponTypes;
    protected ArrayList<ArmorType> validArmorTypes;

    public Hero(String heroName) {
        name = heroName;
        level = 1;
        equipment = new HashMap<>();
        equipment.put(Slot.WEAPON, null);
        equipment.put(Slot.HEAD, null);
        equipment.put(Slot.BODY, null);
        equipment.put(Slot.LEGS, null);
        levelAttributes = new HeroAttribute();
        validArmorTypes = new ArrayList<>();
        validWeaponTypes = new ArrayList<>();
    }

    public abstract void levelUp();

    public void increaseAttributes(int addedStrength, int addedDexterity, int addedIntelligence) {
        level++;
        levelAttributes.increaseStrength(addedStrength);
        levelAttributes.increaseDexterity(addedDexterity);
        levelAttributes.increaseIntelligence(addedIntelligence);
    }

    public void equipArmor(Armor armor) throws InvalidArmorException {
        if (level < armor.getRequiredLevel())
            throw new InvalidArmorException(name + " is too low level to equip this armor!");

        if (!validArmorTypes.contains(armor.getType()))
            throw new InvalidArmorException(name + " cannot equip this type of armor!");

        equipment.put(armor.getSlot(), armor);
    }

    public void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (level < weapon.getRequiredLevel())
            throw new InvalidWeaponException(name + " is too low level to equip this weapon");

        if (!validWeaponTypes.contains(weapon.getType()))
            throw new InvalidWeaponException(name + " cannot equip this type of weapon");

        equipment.put(Slot.WEAPON, weapon);
    }

    public int damage() {

        double damageAttribute = specialityDamage();
        Weapon weapon = (Weapon) equipment.get(Slot.WEAPON);
        if (weapon == null) return (int) (1 + (damageAttribute / 100.0));
        double weaponDamage = weapon.getWeaponDamage();

        return (int) (weaponDamage * (1 + (damageAttribute / 100.0)));
    }

    public abstract int specialityDamage();

    public int totalAttributes() {
        int sum = levelAttributes.getTotalAttributes();
        for (Slot slot : equipment.keySet()) {
            if (slot != Slot.WEAPON) {
                Armor armor = (Armor) equipment.get(slot);
                if (armor != null)
                    sum += armor.getArmorAttribute().getTotalAttributes();
            }
        }
        return sum;
    }

    public int totalStrengthAttributes() {
        int sum = levelAttributes.getStrength();
        for (Slot slot : equipment.keySet()) {
            if (slot != Slot.WEAPON) {
                Armor armor = (Armor) equipment.get(slot);
                if (armor != null)
                    sum += armor.getArmorAttribute().getStrength();
            }
        }
        return sum;
    }

    public int totalDexterityAttributes() {
        int sum = levelAttributes.getDexterity();
        for (Slot slot : equipment.keySet()) {
            if (slot != Slot.WEAPON) {
                Armor armor = (Armor) equipment.get(slot);
                if (armor != null)
                    sum += armor.getArmorAttribute().getDexterity();
            }
        }
        return sum;
    }

    public int totalIntelligenceAttributes() {
        int sum = levelAttributes.getIntelligence();
        for (Slot slot : equipment.keySet()) {
            if (slot != Slot.WEAPON) {
                Armor armor = (Armor) equipment.get(slot);
                if (armor != null)
                    sum += armor.getArmorAttribute().getIntelligence();
            }
        }
        return sum;
    }

    public String display() {
        return this.toString();
    }

    @Override
    public String toString() {
        return "Your hero: {" +
                "Name='" + name + '\'' +
                ", Class=" + this.getClass().getSimpleName() +
                ", Level=" + level +
                ", Total strength=" +  totalStrengthAttributes() +
                ", Total dexterity=" +  totalDexterityAttributes() +
                ", Total intelligence=" + + totalIntelligenceAttributes() +
                ", Damage=" + damage() + '}';
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public HeroAttribute getLevelAttributes() {
        return levelAttributes;
    }


    public Armor getArmor(Slot slot) {
        return (Armor) equipment.get(slot);
    }

    public Weapon getWeapon() {
        return (Weapon) equipment.get(Slot.WEAPON);
    }
}
