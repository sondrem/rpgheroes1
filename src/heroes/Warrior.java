package heroes;

public class Warrior extends Hero {

    public Warrior(String name) {
        super(name);
        levelAttributes.increaseStrength(5);
        levelAttributes.increaseDexterity(2);
        levelAttributes.increaseIntelligence(1);
        validWeaponTypes.add(WeaponType.AXE);
        validWeaponTypes.add(WeaponType.HAMMER);
        validWeaponTypes.add(WeaponType.SWORD);
        validArmorTypes.add(ArmorType.MAIL);
        validArmorTypes.add(ArmorType.PLATE);
    }

    @Override
    public void levelUp() {
        increaseAttributes(3, 2, 1);
    }

    @Override
    public int specialityDamage() {
        return levelAttributes.getStrength();
    }
}
