import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import heroes.*;
import items.Armor;
import items.Weapon;
import utils.HeroAttribute;

public class ExWarriors {
    public static void main(String[] args) {
        Mage mage = new Mage("Gandalf");
        Weapon weapon = new Weapon("WAND", 1, WeaponType.WAND, 100);
        HeroAttribute ha = new HeroAttribute();
        ha.increaseStrength(10);
        Armor armor = new Armor("Shield", 1, Slot.BODY, ArmorType.CLOTH, ha);
        try {
            mage.equipWeapon(weapon);
        } catch (InvalidWeaponException e) {
            System.out.println(e);
        }

        try {
            mage.equipArmor(armor);
        } catch (InvalidArmorException e) {
            System.out.println(e);
        }
        mage.levelUp();
        mage.levelUp();
        System.out.println(mage.display());
        System.out.println(mage.totalAttributes());
    }
}
