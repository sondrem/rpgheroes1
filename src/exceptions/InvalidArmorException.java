package exceptions;

public class InvalidArmorException extends Exception {
    private final String message;

    public InvalidArmorException(String message) {
        this.message = message;
    }

    public String toString() {
        return ("Invalid armor choice:" + message);
    }

    @Override
    public String getMessage() {
        return message;
    }
}
