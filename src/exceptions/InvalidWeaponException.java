package exceptions;

public class InvalidWeaponException extends Exception {
    private final String message;

    public InvalidWeaponException(String message) {
        this.message = message;
    }

    public String toString() {
        return ("Invalid weapon choice:" + message);
    }

    @Override
    public String getMessage() {
        return message;
    }
}
