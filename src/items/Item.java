package items;

import heroes.Slot;

import java.util.Objects;

public abstract class Item {
    protected String name;
    protected int requiredLevel;
    protected Slot slot;

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public Slot getSlot() {
        return slot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return requiredLevel == item.requiredLevel && Objects.equals(name, item.name) && slot == item.slot;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, requiredLevel, slot);
    }
}
