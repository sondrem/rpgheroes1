package items;

import heroes.ArmorType;
import heroes.Slot;
import utils.HeroAttribute;

import java.util.Objects;

public class Armor extends Item {

    private final HeroAttribute armorAttribute;
    private final ArmorType type;

    public Armor(String name, int requiredLevel, Slot slot, ArmorType type, HeroAttribute armorAttribute) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
        this.type = type;
        this.armorAttribute = armorAttribute;
    }

    public HeroAttribute getArmorAttribute() {
        return armorAttribute;
    }

    public ArmorType getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Armor armor = (Armor) o;
        return Objects.equals(armorAttribute, armor.armorAttribute) && type == armor.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), armorAttribute, type);
    }
}
