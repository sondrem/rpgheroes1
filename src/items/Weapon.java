package items;

import heroes.Slot;
import heroes.WeaponType;

import java.util.Objects;

public class Weapon extends Item {
    private final int weaponDamage;
    private final WeaponType type;

    public Weapon(String name, int requiredLevel, WeaponType type, int weaponDamage) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = Slot.WEAPON;
        this.type = type;
        this.weaponDamage = weaponDamage;
    }

    public int getWeaponDamage() {
        return weaponDamage;
    }

    public WeaponType getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Weapon weapon = (Weapon) o;
        return weaponDamage == weapon.weaponDamage && type == weapon.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), weaponDamage, type);
    }
}
